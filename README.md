# Error Prone Maven Tutorial

A quick introduction into using Google's [Error Prone] tool to augment the Java compiler's type analysis.

Please feel free to have a look at [my blog] for the full tutorial.

----

**2015 Micha Kops / hasCode.com**

   [my blog]:http://www.hascode.com/
   [Error Prone]:http://errorprone.info/
